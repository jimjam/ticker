namespace Component {
    /**
     * The grid component
     */
    export class Grid implements PubSub.OnStocksUpdate {
        /**
         * The grid rows
         *
         * @type {GridRow[]}
         */
        private rows: GridRow[] = [];

        /**
         * The grid DOM element
         *
         * @type {HTMLDivElement}
         */
        private _element: HTMLDivElement;

        /**
         * Private constructor
         *
         * @see ::create()
         */
        private constructor() {
            this._element = document.createElement('div');
            this._element.classList.add('grid');
        }

        /**
         * Element accessor
         *
         * @return {HTMLDivElement}
         */
        get element(): HTMLDivElement {
            return this._element;
        }

        /**
         * Create a grid instance
         *
         * We use this factory method to create an instance of the component as
         * we want the Structure.StockCollection to be garbage collected and not
         * defined as a property
         *
         * @param {Structure.StockCollection} stocks
         * @return {Component.Grid}
         */
        public static create(stocks: Structure.StockCollection): Grid {
            const grid: Grid = new this();

            grid.init(stocks);

            return grid;
        }

        /**
         * Initialise the grid
         *
         * @param {Structure.StockCollection} stocks
         */
        private init(stocks: Structure.StockCollection): void {
            // Create the grid header and append it to the grid
            this._element.appendChild(this.createHeader(stocks.header).element);

            // Create the rows
            stocks.stocks.map(stock => this.addRow(this.createStockRow(stock)));
        }

        /**
         * Handle a stocks update
         *
         * @param {string[][]} stocks
         */
        public onStocksUpdate(stocks: string[][]): void {
            stocks.map((stock: string[], stockId: number) => {
                this.rows[stockId].onStockUpdate(stock);
            });
        }

        /**
         * Create the grid header
         *
         * @param {String[]} items
         * @return {GridRow}
         */
        private createHeader(items: string[]): GridRow {
            const header: GridRow = new GridRow({
                classes: ['grid__row__header']
            });

            items.map(text => header.addItem(this.createGridItem(text)));

            return header;
        }

        /**
         * Create a stock row
         *
         * @param {String} stock
         * @return {GridRow}
         */
        private createStockRow(stock: string[]): GridRow {
            const row: GridRow = new GridRow();

            stock.map(text => row.addItem(this.createGridItem(text)));

            return row;
        }

        /**
         * Create a grid item
         *
         * @param {String} text
         * @return {GridItem}
         */
        private createGridItem(text: string): GridItem {
            return new GridItem({ text });
        }

        /**
         * Add a row to the grid
         *
         * @param {GridRow} row
         */
        private addRow(row: GridRow): void {
            this.rows.push(row);
            this._element.appendChild(row.element);
        }
    }
}
