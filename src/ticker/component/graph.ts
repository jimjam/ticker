import structs = Structure.Graph;

namespace Component {
    export class Graph implements PubSub.OnStocksUpdate {
        /**
         * The canvas DOM element
         */
        private _element: HTMLCanvasElement;

        /**
         * The canvas context
         */
        private _context: CanvasRenderingContext2D;

        /**
         * The stock items
         */
        private stockItems: structs.StockItems;

        /**
         * Canvas width and height
         */
        private readonly width: number = 900;
        private readonly height: number = 450;

        /**
         * Graph width and height (the body of the graph not including axis and labels)
         */
        private readonly gWidth: number = 800;
        private readonly gHeight: number = 400;

        /**
         * Private constructor
         *
         * @see ::create()
         */
        private constructor() {
            this._element = document.createElement('canvas');
            this._element.classList.add('graph');

            // Double the drawing size and scale down for prettiness
            this._element.width = this.width * 2;
            this._element.height = this.height * 2;
            this._element.style.width = `${this.width}px`;
            this._element.style.height = `${this.height}px`;

            this._context = this._element.getContext('2d');
            this._context.scale(2,2);

            this.stockItems = new structs.StockItems();
        }

        /**
         * Element accessor
         *
         * @return {HTMLCanvasElement}
         */
        get element(): HTMLCanvasElement {
            return this._element;
        }

        /**
         * Create a graph instance
         *
         * We use this factory method to create an instance of the component as
         * we want the Structure.StockCollection to be garbage collected and not
         * defined as a property
         *
         * @param {Structure.StockCollection} stocks
         * @return {Component.Graph}
         */
        public static create(stocks: Structure.StockCollection): Graph {
            const graph: Graph = new this();

            graph.init(stocks);

            return graph;
        }

        /**
         * Initialise the graph
         *
         * @param {Structure.StockCollection} stocks
         */
        private init(stocks: Structure.StockCollection): void {
            const time: Date = new Date();

            // Iterate the stocks and build the stock items
            stocks.stocks.map(stock => {
                this.stockItems.add(new structs.StockItem({
                    name: stock[0],
                    color: this.getRandomColor(),
                    price: parseFloat(stock[2]),
                    time,
                }));
            });

            this.render();
        }

        /**
         * Handle a stocks update
         *
         * @param {string[][]} stocks
         */
        public onStocksUpdate(stocks: string[][]): void {
            const time: Date = new Date();

            stocks.map((updates, stockId) => {
                this.stockItems.getStockById(stockId).addPriceUpdate(time, updates[2]);
            });

            this._context.clearRect(0, 0 , this.width, this.height);

            this.render();
        }

        /**
         * Render the graph
         */
        private render(): void {
            this.writeAxisText();
            this.drawAxis();
            this.drawGraph();
        }

        /**
         * Draw the graph
         */
        private drawGraph(): void {
            const maxPrice: number = this.getMaxPrice();
            const timePos = this.writeXAxisLabels(this.stockItems.getTimes());
            this.writeYAxisLabels(maxPrice);
            this.drawStockPriceLines(timePos, maxPrice);
        }

        /**
         * Draw the stock price lines
         *
         * @param {Object} timePosX
         * @param {Number} maxPrice
         */
        private drawStockPriceLines(timePosX: {[time: string]: number}, maxPrice: number): void {
            this.stockItems.each(item => this.drawStockPriceLine(item, timePosX, maxPrice));
        }

        /**
         * Draw a stock price line
         *
         * @param {Structure.Graph.StockItem} item
         * @param {Object} timePosX
         * @param {Number} maxPrice
         */
        private drawStockPriceLine(item: structs.StockItem, timePosX: {[time: string]: number}, maxPrice: number): void {
            let xPos = this.width - this.gWidth;
            let yPos = this.gHeight;

            let i = 0,
                itemCount = Object.keys(item.prices).length;

            for (let time in item.prices) {
                this._context.beginPath();
                this._context.moveTo(xPos, yPos);

                // console.log(xPos, yPos);

                yPos = this.gHeight - (item.prices[time].price * (this.gHeight / maxPrice));

                this._context.lineTo(timePosX[time], yPos);
                this._context.strokeStyle = item.color;
                this._context.stroke();

                xPos = timePosX[time];

                if (++i === itemCount) {
                    const nameTxt = this._context.measureText(item.name);
                    this._context.fillStyle = item.color;
                    this._context.fillText(item.name, (xPos - nameTxt.width) - 2, yPos);
                    this._context.fillStyle = '#000000';
                }
            }
        }

        /**
         * Write the X axis labels (and guidelines)
         *
         * @param {Date[]} times
         * @return {Object}
         */
        private writeXAxisLabels(times: Date[]): {[time: string]: number} {
            const xStartPos = this.width - this.gWidth;
            const yPos = this.gHeight + ((this.height - this.gHeight) / 2);
            const xGap = this.gWidth / times.length;
            let gap = xGap;
            let timePositions: {[time: string]: number} = {};

            for (let time of times) {
                const timeFormat = time.toISOString().slice(-13, -1);
                const tsTxt = this._context.measureText(timeFormat);
                const xPos = (xStartPos + gap);

                this._context.fillText(timeFormat, (xPos - (tsTxt.width / 2)), yPos);

                this._context.beginPath();
                this._context.moveTo(xPos, this.gHeight);
                this._context.lineTo(xPos, 0);
                this._context.strokeStyle = '#f7f7f7';
                this._context.stroke();

                timePositions[time.getTime()] = xPos;

                gap += xGap;
            }

            return timePositions;
        }

        /**
         * Write the Y axis labels (and guidelines)
         *
         * @param {Number} maxPrice
         */
        private writeYAxisLabels(maxPrice: number): void {
            let yPos = 10;
            const yAxisXPos = this.width - this.gWidth;
            const yGap = ((this.gHeight - yPos)/maxPrice) * 100;

            this._context.beginPath();

            for (let label = maxPrice; label >= 0; label -= 100) {
                if (label !== 0) {
                    this._context.moveTo(yAxisXPos, yPos);
                    this._context.lineTo(this.width, yPos);
                    this._context.strokeStyle = '#f7f7f7';
                }

                this._context.fillText(label.toString(), 40, yPos);
                yPos += yGap;
            }

            this._context.stroke();
        }

        /**
         * Draw the axis
         */
        private drawAxis(): void {
            const xPos = this.width - this.gWidth;

            this._context.beginPath();
            this._context.moveTo(xPos, 0);
            this._context.lineTo(xPos, this.gHeight);
            this._context.lineTo(this.width, this.gHeight);
            this._context.strokeStyle = '#777777';
            this._context.stroke();
        }

        /**
         * Write the axis texts
         */
        private writeAxisText(): void {
            // X-axis text
            let xTxt = this._context.measureText('Time');
            this._context.fillText('Time', (this.width / 2) - (xTxt.width / 2), this.height);
            this._context.save();
            // Y-axis text
            this._context.rotate(-Math.PI / 2);
            this._context.fillText('Unit Price', (this.height / 2) * -1, 10);
            this._context.restore();
        }

        /**
         * Get the max price from the stock items rounded up to the nearest 100
         *
         * @return {Number}
         */
        private getMaxPrice(): number {
            return Math.ceil(this.stockItems.getMaxPrice() / 100) * 100;
        }

        /**
         * Generate a random hex color
         *
         * @return {String}
         */
        private getRandomColor(): string {
            let color: string = '#';
            const values: string = "ABCDEF1234567890";

            for (let i = 0; i < 6; ++i) {
                color += values.charAt(Math.floor(Math.random() * values.length));
            }

            return color;
        }
    }
}
