namespace Component {
    /**
     * Describes the options available for a grid item
     */
    export interface GridItemOptions {
        text: string;
    }

    /**
     * Component representing an item that resides within a grid row
     */
    export class GridItem {
        /**
         * The DOM element
         */
        public element: HTMLDivElement;

        /**
         * Timer ID set by setTimeout
         */
        private timerId: number;

        /**
         * The classes used
         */
        private readonly classes = {
            base: 'grid__row__column',
            up: 'grid__row__column--green',
            down: 'grid__row__column--red',
        };

        /**
         * Constructor
         *
         * @param {GridItemOptions} options
         */
        constructor(options: GridItemOptions) {
            this.element = document.createElement('div');
            this.element.classList.add(this.classes.base);
            this.element.textContent = options.text;
        }

        /**
         * On update handler
         *
         * @param {String} value The new value
         */
        public onUpdate(value: string): void {
            if (value.length && value != this.element.textContent) {
                this.updateValue(value);
            }
        }

        /**
         * Update the elements value.
         *
         * A red or green class is also added (with a timeout) for when an value
         * has increased or decreased
         *
         * @param {String} value  The new value
         */
        private updateValue(value: string): void {
            if (this.timerId) {
                this.reset();
            }

            const className = this.hasIncreased(value) ? this.classes.up : this.classes.down;

            this.element.classList.add(className);
            this.element.textContent = value;

            this.timerId = setTimeout(() => {
                this.element.classList.remove(className);
                this.timerId = null;
            }, 1000);
        }

        /**
         * Has the new value increased?
         *
         * @param {String} value The new value
         * @return {Boolean}
         */
        private hasIncreased(value): boolean {
            return this.toFloat(value) > this.toFloat(this.element.textContent);
        }

        /**
         * Cast a subject which may contain letters or symbols to a float
         *
         * @param {String} subject
         * @return {Number}
         */
        private toFloat(subject: string): number {
            return parseFloat(subject.replace(/[a-z%]/gi, ''));
        }

        /**
         * Reset the component to its original state
         */
        private reset(): void {
            this.element.classList.remove(this.classes.up, this.classes.down);
            clearTimeout(this.timerId);
            this.timerId = null;
        }
    }
}
