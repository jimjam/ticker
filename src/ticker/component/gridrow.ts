namespace Component {
    /**
     * Describes the options available for a grid row
     */
    export interface GridRowOptions {
        classes?: string[];
    }

    /**
     * Component representing a row on a grid
     */
    export class GridRow {
        /**
         * The grid items
         */
        public items: GridItem[] = [];

        /**
         * The DOM element
         */
        public element: HTMLDivElement;

        /**
         * Constructor
         *
         * @param {GridRowOptions} options (optional)
         */
        constructor(options?: GridRowOptions) {
            this.element = document.createElement('div');
            this.element.classList.add('grid__row');

            if (options && options.classes) {
                this.element.classList.add(...options.classes);
            }
        }

        /**
         * On stock update handler
         *
         * @param {String[]} updates
         */
        public onStockUpdate(updates: string[]): void {
            updates.map((value: string, itemId: number) => {
                this.items[itemId].onUpdate(value);
            });
        }

        /**
         * Add an item to the grid row
         *
         * @param {GridItem} item
         */
        public addItem(item: GridItem): void {
            this.items.push(item);
            this.element.appendChild(item.element);
        }
    }
}
