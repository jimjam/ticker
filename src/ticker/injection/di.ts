namespace Injection {
    export class Di {
        /**
         * Singleton instances
         */
        private instances: {[key: string]: any} = {};

        /**
         * Get a cached instance
         *
         * @param {String} key
         * @param {Function} cb
         */
        private get(key: string, cb: () => any): any {
            if (!this.instances[key]) {
                this.instances[key] = cb();
            }

            return this.instances[key];
        }

        /**
         * Get the snap shot parser
         *
         * @return {Parser.SnapShot}
         */
        get snapShotParser(): Parser.SnapShot {
            return this.get('Parser.SnapShot', () => new Parser.SnapShot());
        }

        /**
         * Get the snap shot parser
         *
         * @return {Parser.Delta}
         */
        get deltaParser(): Parser.Delta {
            return this.get('Parser.Delta', () => new Parser.Delta());
        }

        /**
         * Get the subscriber instance
         *
         * @return {PubSub.Sub}
         */
        get subscriber(): PubSub.Sub {
            return this.get('PubSub.Sub', () => new PubSub.Sub());
        }

        /**
         * Get the publisher instance
         *
         * @return {PubSub.Pub}
         */
        get publisher(): PubSub.Pub {
            return this.get('PubSub.Pub', () => new PubSub.Pub());
        }
    }
}
