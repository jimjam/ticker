namespace Parser {
    /**
     * Parser interface
     *
     * Describes a parser
     */
    export interface ParserInterface {
        /**
         * Instructs the parser to parse a string data set
         *
         * @param {String} data
         * @return {Promise<*>}
         */
        parse(data: string): Promise<any>;
    }

    /**
     * Abstract parser
     */
    export abstract class Parser {
        // Regex to split a string at a line end
        private readonly lineRegex: RegExp = /\r\n|[\n\v\f\r\x85\u2028\u2029]/g;

        // Column separator value
        protected readonly columnSeparator: string = ',';

        /**
         * Split a subject string at the line break delimiter
         *
         * @param {String} subject
         * @return {String[]}
         */
        protected getRows(subject: string): string[] {
            return subject.split(this.lineRegex);
        }

        /**
         * Split a row into columns
         *
         * @param {String} row
         * @return {String[]}
         */
        protected getColumns(row: string): string[] {
            if (!row.length) {
                return [];
            }

            return row.split(this.columnSeparator);
        }
    }
}
