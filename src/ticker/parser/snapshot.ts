namespace Parser {
    /**
     * SnapShot parser
     *
     * Parses a CSV input from a snapshot into a Structure.StockCollection
     */
    export class SnapShot extends Parser implements ParserInterface {
        /**
         * Parse a CSV snapshot
         *
         * @param {String} data
         * @return {Promise<Structure.StockCollection>}
         */
        public parse(data: string): Promise<Structure.StockCollection> {
            const lines: string[] = this.getRows(data);

            let header: string[],
                stocks: string[][] = [],
                columns;

            for (let i = 0; i < lines.length; i++) {
                columns = this.getColumns(lines[i]);

                if (!columns.length) {
                    continue;
                }

                if (i === 0) {
                    header = columns;
                } else {
                    stocks.push(columns);
                }
            }

            return Promise.resolve(new Structure.StockCollection(header, stocks));
        }
    }
}
