namespace Parser {
    export interface DeltaInterface {
        updates: string[][];
        sleep: number;
    }

    export class Delta extends Parser implements ParserInterface {
        /**
         * Parse the CSV deltas
         *
         * @param {String} data
         * @return {Promise<DeltaInterface[]>}
         */
        public parse(data: string) : Promise<DeltaInterface[]> {
            const lines: string[] = this.getRows(data);

            let delta: DeltaInterface = {
                sleep: 0,
                updates: [],
            };

            let deltas: DeltaInterface[] = [delta];

            for (let i = 0; i < lines.length; i++) {
                if (!lines[i].length) {
                    continue;
                }

                if (lines[i].indexOf(this.columnSeparator) < 0) {
                    delta = {
                        sleep: (delta.sleep + parseInt(lines[i])),
                        updates: [],
                    };
                    deltas.push(delta);
                } else {
                    delta.updates.push(this.getColumns(lines[i]));
                }
            }

            return Promise.resolve(deltas);
        }
    }
}
