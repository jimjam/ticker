namespace Structure {
    /**
     * Represents a collection of stocks
     */
    export class StockCollection {
        /**
         * Constructor
         *
         * @param {String[]} header
         * @param {String[][]} stocks
         */
        constructor(public header: string[], public stocks: string[][]) {
        }
    }
}
