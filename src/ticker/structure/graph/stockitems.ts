namespace Structure {
    export namespace Graph {
        /**
         * A collection of stock items used by the graph component
         */
        export class StockItems {
            /**
             * @type {StockItem[]}
             */
            private stocks: StockItem[] = [];

            /**
             * Add a stock item
             *
             * @param {StockItem} stockItem
             */
            public add(stockItem: StockItem): void {
                this.stocks.push(stockItem);
            }

            /**
             * Apply a callback function to each stock item
             *
             * @param {Function} cb
             */
            public each(cb: (item: StockItem) => void): void {
                this.stocks.map(stock => cb(stock));
            }

            /**
             * Get a stock given its name
             *
             * @param {Number} id
             */
            public getStockById(id: number): StockItem {
                return this.stocks[id];
            }

            /**
             * Get the maximum stock price
             *
             * @return {number}
             */
            public getMaxPrice(): number {
                let maxPrice = 0;

                for (let name in this.stocks) {
                    for (let time in this.stocks[name].prices) {
                        if (this.stocks[name].prices[time].price > maxPrice) {
                            maxPrice = this.stocks[name].prices[time].price;
                        }
                    }
                }

                return maxPrice;
            }

            /**
             * Get the times
             *
             * @return {Date[]}
             */
            public getTimes(): Date[] {
                let times: Date[] = [];

                const first = this.stocks[Object.keys(this.stocks)[0]].prices;

                for (let ts in first) {
                    times.push(first[ts].time);
                }

                return times;
            }
        }

    }
}
