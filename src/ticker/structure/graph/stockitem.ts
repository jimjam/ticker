namespace Structure {
    export namespace Graph {
        export interface StockItemPriceInterface {
            price: number;
            time: Date;
        }

        export interface StockItemInterface extends StockItemPriceInterface {
            name: string;
            color: string;
        }

        /**
         * Describes a stock item
         */
        export class StockItem {
            private _name: string;
            private _color: string;
            private _prices: { [time: number]: StockItemPriceInterface } = {};

            /**
             * Constructor
             *
             * @param {StockItemInterface} item
             */
            constructor(item: StockItemInterface) {
                this._name = item.name;
                this._color = item.color;
                this._prices[item.time.getTime()] = {
                    price: item.price,
                    time: item.time,
                };
            }

            /**
             * Name accessor
             *
             * @return {String}
             */
            get name(): string {
                return this._name;
            }

            /**
             * Color accessor
             *
             * @return {String}
             */
            get color(): string {
                return this._color;
            }

            /**
             * Prices accessor
             *
             * @return {Object}
             */
            get prices(): {[time: number]: StockItemPriceInterface} {
                return this._prices;
            }

            /**
             * Add a price to the stock item
             *
             * If the price did not get updated then set the price for the time
             * period to the latest price
             *
             * @param {Date} time
             * @param {String} value
             */
            public addPriceUpdate(time: Date, value: string): void {
                const price: number = parseFloat(value);

                if (!isNaN(price)) {
                    this.prices[time.getTime()] = { time, price };
                } else {
                    const latest: number = Math.max.apply(Math, Object.keys(this.prices));

                    this.prices[time.getTime()] = {
                        time,
                        price: this.prices[latest].price
                    };
                }
            }
        }
    }
}
