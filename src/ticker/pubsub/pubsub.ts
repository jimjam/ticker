namespace PubSub {
    /**
     * Describes the stocks update handler
     */
    export interface OnStocksUpdate {
        /**
         * On stocks update handler
         *
         * @param {string[][]} stocks
         */
        onStocksUpdate(stocks: string[][]): void;
    }

    // Registered subscribers
    let subs: { [key: string]: any } = {};

    /**
     * Publisher
     */
    export class Pub {
        public publish(key: string, value: any): void {
            if (subs[key]) {
                subs[key].forEach(sub => sub(value));
            }
        }
    }

    /**
     * Subscriber
     */
    export class Sub {
        public subscribe(key: string, cb: (value: any) => void): void {
            if (!subs[key]){
                subs[key] = [cb];
            } else {
                subs[key].push(cb);
            }
        }
    }
}
