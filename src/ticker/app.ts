namespace Ticker {
    export class App {
        /**
         * On stocks update queue name
         */
        private readonly updatesQueue: string = 'onStocksUpdate';

        /**
         * Constructor
         *
         * @param {Injection.di} di
         */
        constructor(private di?: Injection.Di) {
            if (!di) {
                this.di = new Injection.Di();
            }
        }

        /**
         * Run the ticker app
         *
         * - Fetch the snapshot
         * - Generate the components
         * - Attach the component's elements to the DOM (root element)
         * - Fetch the deltas
         * - Update the components
         *
         * @param {HTMLElement} rootElement
         */
        public run(rootElement: HTMLElement): void {
            this.getSnapshot()
                .then(stocks => this.onSnapshot(stocks))
                .then(elements => elements.map(element => rootElement.appendChild(element)))
                .then(() => this.getDeltas())
                .then(deltas => this.onDeltas(deltas));
        }

        /**
         * On deltas handler
         *
         * - Iterate the deltas
         * - Publish the updates to the update queue once the timeout has been reached
         * - If all deltas have been processed then re-iterate
         *
         * @param {Parser.DeltaInterface[]} deltas
         */
        private onDeltas(deltas: Parser.DeltaInterface[]): void {
            deltas.map((delta, key) => {
                setTimeout(() => {
                    this.di.publisher.publish(this.updatesQueue, delta.updates);

                    if (++key === deltas.length) {
                        this.onDeltas(deltas);
                    }
                }, delta.sleep);
            });
        }

        /**
         * On snapshot handler
         *
         * - Create the components and subscribe their listeners to the updates queue
         *
         * @param {Structure.StockCollection} stocks
         * @return {Promise<HTMLElement[]>}
         */
        private onSnapshot(stocks: Structure.StockCollection) : Promise<HTMLElement[]> {
            const grid: Component.Grid = Component.Grid.create(stocks);
            const graph: Component.Graph = Component.Graph.create(stocks);

            this.di.subscriber.subscribe(this.updatesQueue, stocks => grid.onStocksUpdate(stocks));
            this.di.subscriber.subscribe(this.updatesQueue, stocks => graph.onStocksUpdate(stocks));

            return Promise.resolve([
                grid.element,
                graph.element,
            ]);
        }

        /**
         * Get the snapshot
         *
         * @return {Promise<Structure.StockCollection>}
         */
        private getSnapshot() : Promise<Structure.StockCollection> {
            return fetch('assets/snapshot.csv')
                .then(response => response.text())
                .then(data => this.di.snapShotParser.parse(data));
        }

        /**
         * Get the deltas
         *
         * @return {Promise<Parser.DeltaInterface[]>}
         */
        private getDeltas(): Promise<Parser.DeltaInterface[]> {
            return fetch('assets/deltas.csv')
                .then(response => response.text())
                .then(data => this.di.deltaParser.parse(data));
        }
    }
}
