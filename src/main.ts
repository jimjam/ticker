/// <reference path="ticker/app.ts" />
/// <reference path="ticker/injection/di.ts" />
/// <reference path="ticker/parser/parser.ts" />
/// <reference path="ticker/parser/snapshot.ts" />
/// <reference path="ticker/parser/delta.ts" />
/// <reference path="ticker/pubsub/pubsub.ts" />
/// <reference path="ticker/structure/stockcollection.ts" />
/// <reference path="ticker/structure/graph/stockitem.ts" />
/// <reference path="ticker/structure/graph/stockitems.ts" />
/// <reference path="ticker/component/griditem.ts" />
/// <reference path="ticker/component/gridrow.ts" />
/// <reference path="ticker/component/grid.ts" />
/// <reference path="ticker/component/graph.ts" />

(() => new Ticker.App().run(document.getElementById('ticker')))();
