var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Ticker;
(function (Ticker) {
    var App = (function () {
        function App(di) {
            this.di = di;
            this.updatesQueue = this.updatesQueue;
            if (!di) {
                this.di = new Injection.Di();
            }
        }
        App.prototype.run = function (rootElement) {
            var _this = this;
            this.getSnapshot()
                .then(function (stocks) { return _this.onSnapshot(stocks); })
                .then(function (elements) { return elements.map(function (element) { return rootElement.appendChild(element); }); })
                .then(function () { return _this.getDeltas(); })
                .then(function (deltas) { return _this.onDeltas(deltas); });
        };
        App.prototype.onDeltas = function (deltas) {
            var _this = this;
            deltas.map(function (delta, key) {
                setTimeout(function () {
                    _this.di.publisher.publish(_this.updatesQueue, delta.updates);
                    if (++key === deltas.length) {
                        _this.onDeltas(deltas);
                    }
                }, delta.sleep);
            });
        };
        App.prototype.onSnapshot = function (stocks) {
            var grid = Component.Grid.create(stocks);
            var graph = Component.Graph.create(stocks);
            this.di.subscriber.subscribe(this.updatesQueue, function (stocks) { return grid.onStocksUpdate(stocks); });
            this.di.subscriber.subscribe(this.updatesQueue, function (stocks) { return graph.onStocksUpdate(stocks); });
            return Promise.resolve([
                grid.element,
                graph.element,
            ]);
        };
        App.prototype.getSnapshot = function () {
            var _this = this;
            return fetch('assets/snapshot.csv')
                .then(function (response) { return response.text(); })
                .then(function (data) { return _this.di.snapShotParser.parse(data); });
        };
        App.prototype.getDeltas = function () {
            var _this = this;
            return fetch('assets/deltas.csv')
                .then(function (response) { return response.text(); })
                .then(function (data) { return _this.di.deltaParser.parse(data); });
        };
        return App;
    }());
    Ticker.App = App;
})(Ticker || (Ticker = {}));
var Injection;
(function (Injection) {
    var Di = (function () {
        function Di() {
            this.instances = {};
        }
        Di.prototype.get = function (key, cb) {
            if (!this.instances[key]) {
                this.instances[key] = cb();
            }
            return this.instances[key];
        };
        Object.defineProperty(Di.prototype, "snapShotParser", {
            get: function () {
                return this.get('Parser.SnapShot', function () { return new Parser.SnapShot(); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Di.prototype, "deltaParser", {
            get: function () {
                return this.get('Parser.Delta', function () { return new Parser.Delta(); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Di.prototype, "subscriber", {
            get: function () {
                return this.get('PubSub.Sub', function () { return new PubSub.Sub(); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Di.prototype, "publisher", {
            get: function () {
                return this.get('PubSub.Pub', function () { return new PubSub.Pub(); });
            },
            enumerable: true,
            configurable: true
        });
        return Di;
    }());
    Injection.Di = Di;
})(Injection || (Injection = {}));
var Parser;
(function (Parser_1) {
    var Parser = (function () {
        function Parser() {
            this.lineRegex = /\r\n|[\n\v\f\r\x85\u2028\u2029]/g;
            this.columnSeparator = ',';
        }
        Parser.prototype.getRows = function (subject) {
            return subject.split(this.lineRegex);
        };
        Parser.prototype.getColumns = function (row) {
            if (!row.length) {
                return [];
            }
            return row.split(this.columnSeparator);
        };
        return Parser;
    }());
    Parser_1.Parser = Parser;
})(Parser || (Parser = {}));
var Parser;
(function (Parser) {
    var SnapShot = (function (_super) {
        __extends(SnapShot, _super);
        function SnapShot() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SnapShot.prototype.parse = function (data) {
            var lines = this.getRows(data);
            var header, stocks = [], columns;
            for (var i = 0; i < lines.length; i++) {
                columns = this.getColumns(lines[i]);
                if (!columns.length) {
                    continue;
                }
                if (i === 0) {
                    header = columns;
                }
                else {
                    stocks.push(columns);
                }
            }
            return Promise.resolve(new Structure.StockCollection(header, stocks));
        };
        return SnapShot;
    }(Parser.Parser));
    Parser.SnapShot = SnapShot;
})(Parser || (Parser = {}));
var Parser;
(function (Parser) {
    var Delta = (function (_super) {
        __extends(Delta, _super);
        function Delta() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Delta.prototype.parse = function (data) {
            var lines = this.getRows(data);
            var delta = {
                sleep: 0,
                updates: [],
            };
            var deltas = [delta];
            for (var i = 0; i < lines.length; i++) {
                if (!lines[i].length) {
                    continue;
                }
                if (lines[i].indexOf(this.columnSeparator) < 0) {
                    delta = {
                        sleep: (delta.sleep + parseInt(lines[i])),
                        updates: [],
                    };
                    deltas.push(delta);
                }
                else {
                    delta.updates.push(this.getColumns(lines[i]));
                }
            }
            return Promise.resolve(deltas);
        };
        return Delta;
    }(Parser.Parser));
    Parser.Delta = Delta;
})(Parser || (Parser = {}));
var PubSub;
(function (PubSub) {
    var subs = {};
    var Pub = (function () {
        function Pub() {
        }
        Pub.prototype.publish = function (key, value) {
            if (subs[key]) {
                subs[key].forEach(function (sub) { return sub(value); });
            }
        };
        return Pub;
    }());
    PubSub.Pub = Pub;
    var Sub = (function () {
        function Sub() {
        }
        Sub.prototype.subscribe = function (key, cb) {
            if (!subs[key]) {
                subs[key] = [cb];
            }
            else {
                subs[key].push(cb);
            }
        };
        return Sub;
    }());
    PubSub.Sub = Sub;
})(PubSub || (PubSub = {}));
var Structure;
(function (Structure) {
    var StockCollection = (function () {
        function StockCollection(header, stocks) {
            this.header = header;
            this.stocks = stocks;
        }
        return StockCollection;
    }());
    Structure.StockCollection = StockCollection;
})(Structure || (Structure = {}));
var Structure;
(function (Structure) {
    var Graph;
    (function (Graph) {
        var StockItem = (function () {
            function StockItem(item) {
                this._prices = {};
                this._name = item.name;
                this._color = item.color;
                this._prices[item.time.getTime()] = {
                    price: item.price,
                    time: item.time,
                };
            }
            Object.defineProperty(StockItem.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StockItem.prototype, "color", {
                get: function () {
                    return this._color;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StockItem.prototype, "prices", {
                get: function () {
                    return this._prices;
                },
                enumerable: true,
                configurable: true
            });
            StockItem.prototype.addPriceUpdate = function (time, value) {
                var price = parseFloat(value);
                if (!isNaN(price)) {
                    this.prices[time.getTime()] = { time: time, price: price };
                }
                else {
                    var latest = Math.max.apply(Math, Object.keys(this.prices));
                    this.prices[time.getTime()] = {
                        time: time,
                        price: this.prices[latest].price
                    };
                }
            };
            return StockItem;
        }());
        Graph.StockItem = StockItem;
    })(Graph = Structure.Graph || (Structure.Graph = {}));
})(Structure || (Structure = {}));
var Structure;
(function (Structure) {
    var Graph;
    (function (Graph) {
        var StockItems = (function () {
            function StockItems() {
                this.stocks = [];
            }
            StockItems.prototype.add = function (stockItem) {
                this.stocks.push(stockItem);
            };
            StockItems.prototype.each = function (cb) {
                this.stocks.map(function (stock) { return cb(stock); });
            };
            StockItems.prototype.getStockById = function (id) {
                return this.stocks[id];
            };
            StockItems.prototype.getMaxPrice = function () {
                var maxPrice = 0;
                for (var name_1 in this.stocks) {
                    for (var time in this.stocks[name_1].prices) {
                        if (this.stocks[name_1].prices[time].price > maxPrice) {
                            maxPrice = this.stocks[name_1].prices[time].price;
                        }
                    }
                }
                return maxPrice;
            };
            StockItems.prototype.getTimes = function () {
                var times = [];
                var first = this.stocks[Object.keys(this.stocks)[0]].prices;
                for (var ts in first) {
                    times.push(first[ts].time);
                }
                return times;
            };
            return StockItems;
        }());
        Graph.StockItems = StockItems;
    })(Graph = Structure.Graph || (Structure.Graph = {}));
})(Structure || (Structure = {}));
var Component;
(function (Component) {
    var GridItem = (function () {
        function GridItem(options) {
            this.classes = {
                base: 'grid__row__column',
                up: 'grid__row__column--green',
                down: 'grid__row__column--red',
            };
            this.element = document.createElement('div');
            this.element.classList.add(this.classes.base);
            this.element.textContent = options.text;
        }
        GridItem.prototype.onUpdate = function (value) {
            if (value.length && value != this.element.textContent) {
                this.updateValue(value);
            }
        };
        GridItem.prototype.updateValue = function (value) {
            var _this = this;
            if (this.timerId) {
                this.reset();
            }
            var className = this.hasIncreased(value) ? this.classes.up : this.classes.down;
            this.element.classList.add(className);
            this.element.textContent = value;
            this.timerId = setTimeout(function () {
                _this.element.classList.remove(className);
                _this.timerId = null;
            }, 1000);
        };
        GridItem.prototype.hasIncreased = function (value) {
            return this.toFloat(value) > this.toFloat(this.element.textContent);
        };
        GridItem.prototype.toFloat = function (subject) {
            return parseFloat(subject.replace(/[a-z%]/gi, ''));
        };
        GridItem.prototype.reset = function () {
            this.element.classList.remove(this.classes.up, this.classes.down);
            clearTimeout(this.timerId);
            this.timerId = null;
        };
        return GridItem;
    }());
    Component.GridItem = GridItem;
})(Component || (Component = {}));
var Component;
(function (Component) {
    var GridRow = (function () {
        function GridRow(options) {
            this.items = [];
            this.element = document.createElement('div');
            this.element.classList.add('grid__row');
            if (options && options.classes) {
                (_a = this.element.classList).add.apply(_a, options.classes);
            }
            var _a;
        }
        GridRow.prototype.onStockUpdate = function (updates) {
            var _this = this;
            updates.map(function (value, itemId) {
                _this.items[itemId].onUpdate(value);
            });
        };
        GridRow.prototype.addItem = function (item) {
            this.items.push(item);
            this.element.appendChild(item.element);
        };
        return GridRow;
    }());
    Component.GridRow = GridRow;
})(Component || (Component = {}));
var Component;
(function (Component) {
    var Grid = (function () {
        function Grid() {
            this.rows = [];
            this._element = document.createElement('div');
            this._element.classList.add('grid');
        }
        Object.defineProperty(Grid.prototype, "element", {
            get: function () {
                return this._element;
            },
            enumerable: true,
            configurable: true
        });
        Grid.create = function (stocks) {
            var grid = new this();
            grid.init(stocks);
            return grid;
        };
        Grid.prototype.init = function (stocks) {
            var _this = this;
            this._element.appendChild(this.createHeader(stocks.header).element);
            stocks.stocks.map(function (stock) { return _this.addRow(_this.createStockRow(stock)); });
        };
        Grid.prototype.onStocksUpdate = function (stocks) {
            var _this = this;
            stocks.map(function (stock, stockId) {
                _this.rows[stockId].onStockUpdate(stock);
            });
        };
        Grid.prototype.createHeader = function (items) {
            var _this = this;
            var header = new Component.GridRow({
                classes: ['grid__row__header']
            });
            items.map(function (text) { return header.addItem(_this.createGridItem(text)); });
            return header;
        };
        Grid.prototype.createStockRow = function (stock) {
            var _this = this;
            var row = new Component.GridRow();
            stock.map(function (text) { return row.addItem(_this.createGridItem(text)); });
            return row;
        };
        Grid.prototype.createGridItem = function (text) {
            return new Component.GridItem({ text: text });
        };
        Grid.prototype.addRow = function (row) {
            this.rows.push(row);
            this._element.appendChild(row.element);
        };
        return Grid;
    }());
    Component.Grid = Grid;
})(Component || (Component = {}));
var structs = Structure.Graph;
var Component;
(function (Component) {
    var Graph = (function () {
        function Graph() {
            this.width = 900;
            this.height = 450;
            this.gWidth = 800;
            this.gHeight = 400;
            this._element = document.createElement('canvas');
            this._element.classList.add('graph');
            this._element.width = this.width * 2;
            this._element.height = this.height * 2;
            this._element.style.width = this.width + "px";
            this._element.style.height = this.height + "px";
            this._context = this._element.getContext('2d');
            this._context.scale(2, 2);
            this.stockItems = new structs.StockItems();
        }
        Object.defineProperty(Graph.prototype, "element", {
            get: function () {
                return this._element;
            },
            enumerable: true,
            configurable: true
        });
        Graph.create = function (stocks) {
            var graph = new this();
            graph.init(stocks);
            return graph;
        };
        Graph.prototype.init = function (stocks) {
            var _this = this;
            var time = new Date();
            stocks.stocks.map(function (stock, stockId) {
                _this.stockItems.add(new structs.StockItem({
                    name: stock[0],
                    color: _this.getRandomColor(),
                    price: parseFloat(stock[2]),
                    time: time,
                }));
            });
            this.render();
        };
        Graph.prototype.onStocksUpdate = function (stocks) {
            var _this = this;
            var time = new Date();
            stocks.map(function (updates, stockId) {
                _this.stockItems.getStockById(stockId).addPriceUpdate(time, updates[2]);
            });
            this._context.clearRect(0, 0, this.width, this.height);
            this.render();
        };
        Graph.prototype.render = function () {
            this.writeAxisText();
            this.drawAxis();
            this.drawGraph();
        };
        Graph.prototype.drawGraph = function () {
            var maxPrice = this.getMaxPrice();
            var timePos = this.writeXAxisLabels(this.stockItems.getTimes());
            this.writeYAxisLabels(maxPrice);
            this.drawStockPriceLines(timePos, maxPrice);
        };
        Graph.prototype.drawStockPriceLines = function (timePosX, maxPrice) {
            var _this = this;
            this.stockItems.each(function (item) { return _this.drawStockPriceLine(item, timePosX, maxPrice); });
        };
        Graph.prototype.drawStockPriceLine = function (item, timePosX, maxPrice) {
            var xPos = this.width - this.gWidth;
            var yPos = this.gHeight;
            var i = 0, itemCount = Object.keys(item.prices).length;
            for (var time in item.prices) {
                this._context.beginPath();
                this._context.moveTo(xPos, yPos);
                yPos = this.gHeight - (item.prices[time].price * (this.gHeight / maxPrice));
                this._context.lineTo(timePosX[time], yPos);
                this._context.strokeStyle = item.color;
                this._context.stroke();
                xPos = timePosX[time];
                if (++i === itemCount) {
                    var nameTxt = this._context.measureText(item.name);
                    this._context.fillStyle = item.color;
                    this._context.fillText(item.name, (xPos - nameTxt.width) - 2, yPos);
                    this._context.fillStyle = '#000000';
                }
            }
        };
        Graph.prototype.writeXAxisLabels = function (times) {
            var xStartPos = this.width - this.gWidth;
            var yPos = this.gHeight + ((this.height - this.gHeight) / 2);
            var xGap = this.gWidth / times.length;
            var gap = xGap;
            var timePositions = {};
            for (var _i = 0, times_1 = times; _i < times_1.length; _i++) {
                var time = times_1[_i];
                var timeFormat = time.toISOString().slice(-13, -1);
                var tsTxt = this._context.measureText(timeFormat);
                var xPos = (xStartPos + gap);
                this._context.fillText(timeFormat, (xPos - (tsTxt.width / 2)), yPos);
                this._context.beginPath();
                this._context.moveTo(xPos, this.gHeight);
                this._context.lineTo(xPos, 0);
                this._context.strokeStyle = '#f7f7f7';
                this._context.stroke();
                timePositions[time.getTime()] = xPos;
                gap += xGap;
            }
            return timePositions;
        };
        Graph.prototype.writeYAxisLabels = function (maxPrice) {
            var yPos = 10;
            var yAxisXPos = this.width - this.gWidth;
            var yGap = ((this.gHeight - yPos) / maxPrice) * 100;
            this._context.beginPath();
            for (var label = maxPrice; label >= 0; label -= 100) {
                if (label !== 0) {
                    this._context.moveTo(yAxisXPos, yPos);
                    this._context.lineTo(this.width, yPos);
                    this._context.strokeStyle = '#f7f7f7';
                }
                this._context.fillText(label.toString(), 40, yPos);
                yPos += yGap;
            }
            this._context.stroke();
        };
        Graph.prototype.drawAxis = function () {
            var xPos = this.width - this.gWidth;
            this._context.beginPath();
            this._context.moveTo(xPos, 0);
            this._context.lineTo(xPos, this.gHeight);
            this._context.lineTo(this.width, this.gHeight);
            this._context.strokeStyle = '#777777';
            this._context.stroke();
        };
        Graph.prototype.writeAxisText = function () {
            var xTxt = this._context.measureText('Time');
            this._context.fillText('Time', (this.width / 2) - (xTxt.width / 2), this.height);
            this._context.save();
            this._context.rotate(-Math.PI / 2);
            this._context.fillText('Unit Price', (this.height / 2) * -1, 10);
            this._context.restore();
        };
        Graph.prototype.getMaxPrice = function () {
            return Math.ceil(this.stockItems.getMaxPrice() / 100) * 100;
        };
        Graph.prototype.getRandomColor = function () {
            var color = '#';
            var values = "ABCDEF1234567890";
            for (var i = 0; i < 6; ++i) {
                color += values.charAt(Math.floor(Math.random() * values.length));
            }
            return color;
        };
        return Graph;
    }());
    Component.Graph = Graph;
})(Component || (Component = {}));
(function () { return new Ticker.App().run(document.getElementById('ticker')); })();
//# sourceMappingURL=main.js.map