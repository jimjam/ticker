# Bet365 Tech Test

## Application Architecture

There's a high level overview of the application's architecture in `archetecture.jpeg`.

## Starting the app

I've been using the `http-server` NPM modules to serve the files. This is the only third-party software used.

To run the app:

```
npm install
npm run-script develop
```

Alternatively you can serve the `www/index.html` file from any other webserver.

## Demo

[Click Here For Online Demo](https://unalome.technology)
